# -*- coding: utf-8 -*-
""" Setup file template from pyscaffold, but with pyscaffold dependency removed.

Use setup.cfg and pyproject.toml to configure your project.
"""
import sys

from setuptools import setup


if __name__ == "__main__":
    setup()
